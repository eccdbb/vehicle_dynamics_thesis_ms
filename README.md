# Model Predictive Control for Vehicle Dynamics - Masters Thesis #

## What is this? ##
This is a mirror of code used for my Master's thesis (https://www.nctu.edu.tw/en now https://en.nycu.edu.tw/).  Originally, this project used Simulink and large images; therefore, no git was used.
This git contains an archive of version 20200429a and 20200429b, which is described below.

## 20200429a ##
See log.txt for details.  Note some completed TODO's haven't been converted to DONE.

The overall goal is described in the thesis abstract below.

0200429a contains code for a set of vehicle dynamics MPC problems: P1 to P9.  The locations of P1 to P8 are described in log.txt under the SS/MS table.  P9 is located in "./3 level MPC/".
These problems are fully described in the thesis.  A draft of the thesis is included in this git for convenience, but please do not share it publicly until officially published.
Note: "3 level MPC" is actually implemented as 2 levels (kinematic -> dynamic), since wheel control (actuator level) is assumed to be ideal.

Simulation assumtions, limitations, and issues are described in the thesis.

## 20200429b ##
Slightly updated version.  Used for final publication. 

## Abstract ##
tl;dr:

- One control method for a wide operating envelope

- Start normal, reach large slip, recover to normal...using kinematic-dynamic MPC

Full abstract:


In this thesis, a collection of vehicle dynamics model predictive control (MPC) problems are solved.  The goal is to generate in open loop a sequence of control actions and apply the entire sequence until the end of the prediction horizon.  A planar double track vehicle model with suspension and tire sub-models serves as a reference model.  Then, variations on this model are used for control.  Limitations of the reference model are also discussed.

First, a MPC toolbox is developed to support the single and multiple shooting methods with black box model parallel simulation.  The toolbox is written in MATLAB and uses IPOPT as the solver.

Then, a set of MPC problems are presented, including MPC with the full vehicle model, a simplified model with torque distribution logic, a kinematic model, a dynamic model, and a simple wheel model.  Finally, 2-level MPC using the kinematic and dynamic models is presented.  The results for MPC with the kinematic model, MPC with the dynamic model, and 2-level MPC are shown.  Experiments show that, in addition to normal driving scenarios, the dynamic model predictive controller allows the vehicle to reach large body slip angles (40\textdegree) and return to normal.  For the 2-level MPC, the kinematic controller is able to generate velocity waypoints for the dynamic controller reach.  However, due to the 2-level controller formulation, open-loop operation results in sub-optimal results.

### File
A copy of the thesis is provided here.


## Code Details ##
20200429a/b contains a @MPC class, which implements the problem interface.  It supports single shooting (SS) and multiple shooting (MS) internally.  To add auxilliary constraints, follow the code examples.
The problem vector is defined in the thesis.  Supporting classes @timeline and @environment are used for simulation and logging.

To run, please download the pre-compiled IPOPT .mex from here:
https://projects.coin-or.org/Ipopt/wiki/MatlabInterface

Then, change the addpath() so MATLAB can find it.  Note: for debugging, an updated interface is available here:
https://www.mathworks.com/matlabcentral/fileexchange/53040-ebertolazzi-mexipopt

### Why write a problem interface when many already exist? ###
Why write from scratch when exists: ACADO, CasADi, FORCESPRO, etc.?

I started out using https://www.mathworks.com/matlabcentral/fileexchange/63618-mfeval .  MATLAB Symbolic Math Toolbox does not do automatic differentiation, so I decided to use finite differences to support black box functions.
Once set in stone, a stone is a stone.

## Results ##
Please see the thesis.

## What next? (w.i.p) ##
- Google JAX and https://pypi.org/project/ipopt/ to speed up the computation by a ton

- Make a stochastic MPC tool for GPU

- See thesis conclucion for more

## For more information ##
Vincent Chyn

eccdbbabcdeee@gmail.com

